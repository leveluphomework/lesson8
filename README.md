# lesson8

Build container using kaniko+docker


Задание:
 - Создать docker-контейнер c nginx сервером и сохранить его в гитлаб регистри.

Выполнение:
 - Скрин гитлаб регистри с собраным контейнером.
    https://gitlab.com/leveluphomework/lesson8/-/blob/main/gitlab_registry.png
 - Скрин страницы nginx, запущенного образа.
    https://gitlab.com/leveluphomework/lesson8/-/blob/main/nginx.png


